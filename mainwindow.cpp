#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    hw1 = 0;
    hw2 = 0;
    hw3 = 0;
    hw4 = 0;
    hw5 = 0;
    hw6 = 0;
    hw7 = 0;
    hw8 = 0;
    midterm_sum = 0;
    midterm1 = 0;
    midterm2 = 0;
    final = 0;
    final_project = 0.0;
    clickedA = false;
    clickedB = false;
    pic10B_clicked = false;
    pic10C_clicked = false;
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_hw1slider_valueChanged(int value)
{
    hw1 = value;
}

void MainWindow::on_hw2slider_valueChanged(int value)
{
    hw2 = value;
}

void MainWindow::on_hw3slider_valueChanged(int value)
{
    hw3 = value;
}

// since the PIC10C grading scheme only contains 3 homeworks
// we will set hw4 and succeeding homework values to zero even
// if the user were to change these sliders
// so as long as the PIC10C button is clicked
// this also includes midterm2
void MainWindow::on_hw4slider_valueChanged(int value)
{
   if(pic10B_clicked == true){
       hw4 = value;
   }
   else if(pic10C_clicked == true){
       hw4 = 0;
   }
   else
       return;
}

void MainWindow::on_hw5slider_valueChanged(int value)
{
    if(pic10B_clicked == true){
        hw5 = value;
    }
    else if(pic10C_clicked == true){
        hw5 = 0;
    }
    else
        return;
}

void MainWindow::on_hw6slider_valueChanged(int value)
{
    if(pic10B_clicked == true){
        hw6 = value;
    }
    else if(pic10C_clicked == true){
        hw6 = 0;
    }
    else
        return;
}

void MainWindow::on_hw7slider_valueChanged(int value)
{
    if(pic10B_clicked == true){
        hw7 = value;
    }
    else if(pic10C_clicked == true){
        hw7 = 0;
    }
    else
        return;
}

void MainWindow::on_hw8slider_valueChanged(int value)
{
    if(pic10B_clicked == true){
        hw8 = value;
    }
    else if(pic10C_clicked == true){
        hw8 = 0;
    }
    else
        return;
}

double MainWindow::calculateBestPercent(std::string type){
   if(pic10B_clicked == true){
        double hw_percentage_worth = 0.03125;
        double total_hw = hw_percentage_worth*(hw1 + hw2 + hw3 + hw4 + hw5 + hw6 + hw7 + hw8);
        if(type == "scheme1"){
            double local_sum = total_hw + (midterm1 * 0.2) + (midterm2 * 0.2) + (final * 0.35);
                 return local_sum;
        }
       else if(type == "scheme2"){
            double bestMidterm = 0.0;
            double midterm1percentage = (midterm1 * 0.3);
            double midterm2percentage = (midterm2 * 0.3);
            double final_percentage = (final * 0.44);

            if(midterm1percentage > midterm2percentage)
                bestMidterm = midterm1percentage;
            else
                bestMidterm = midterm2percentage;

            double local_sum2 = total_hw + bestMidterm + final_percentage;
                  return local_sum2;

        }
        else
            return 0.0;
        }
        else if(pic10C_clicked == true){
        double project = (final_project * 0.35);
        double hws = (hw1 * 0.05) + (hw2 * 0.05) + (hw3 * 0.05);
            if(type == "scheme1"){
                double final_percentage = (final * 0.25);
                double midterm1percentage = (midterm1 * 0.25);
                double local_sum = project + hws + midterm1percentage + final_percentage;
                return local_sum;
            }
            else if(type == "scheme2"){
                double final_percentage = (final *0.5);
                double local_sum = project + hws + final_percentage;
                return local_sum;
            }
            else
                return 0.0;
        }
        else
            return 0.0;
}

void MainWindow::on_midterm1slider_valueChanged(int value)
{
    midterm1 = value;
}

void MainWindow::on_midterm2slider_valueChanged(int value)
{
    if(pic10B_clicked == true){
        midterm2 = value;
        final_project = 0.0;
    }
    else if(pic10C_clicked == true){
        midterm2 = 0;
        final_project = value;
    }
    else
        return;
}

void MainWindow::on_finalexamslider_valueChanged(int value)
{
    final = value;
}

// a function to change the value of total course grade
// will update the course grade based on schemeA grading structure
void MainWindow::on_schemeAradioButton_clicked()
{
    clickedA = true;
    clickedB = false;
    double dbl = calculateBestPercent("scheme1");
    QString str = QString("%1").arg(dbl);
    ui->overall_score->setText(str);
}

// will update the course grade based on schemeB grading structure
void MainWindow::on_schemeBradioButton_clicked()
{
    clickedB = true;
    clickedA = false;
    double dbl = calculateBestPercent("scheme2");
    QString str = QString("%1").arg(dbl);
    ui->overall_score->setText(str);
}

// need to change according to scheme
void MainWindow::on_hw1slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw2slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw3slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
        return;
}

void MainWindow::on_hw4slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw5slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw6slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw7slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw8slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_midterm1slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}


void MainWindow::on_midterm2slider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_finalexamslider_sliderMoved()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw1spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw2spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw3spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw4spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw5spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw6spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw7spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_hw8spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_midterm1spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_midterm2spinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_finalexamspinbox_valueChanged()
{
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_PIC10B_button_clicked()
{
    ui->midterm2_label->setText("Midterm 2");

    ui->PIC10B_button->setText("course PIC 10B chosen");
    ui->PIC10C_button->setText("PIC 10C. Advanced Programming.");
    pic10B_clicked = true;
    pic10C_clicked = false;
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}

void MainWindow::on_PIC10C_button_clicked()
{
    ui->midterm2_label->setText("Final Project: ");

    ui->PIC10C_button->setText("course PIC 10C chosen");
    ui->PIC10B_button->setText("PIC 10B. Intermediate Programming.");
    pic10C_clicked = true;
    pic10B_clicked = false;
    if(clickedA == true){
        double dbl = calculateBestPercent("scheme1");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else if(clickedB == true){
        double dbl = calculateBestPercent("scheme2");
        QString str = QString("%1").arg(dbl);
        ui->overall_score->setText(str);
    }
    else
        return;
}
