#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_hw1slider_valueChanged(int value);

    void on_hw2slider_valueChanged(int value);

    void on_hw3slider_valueChanged(int value);

    void on_hw4slider_valueChanged(int value);

    void on_hw5slider_valueChanged(int value);

    void on_hw6slider_valueChanged(int value);

    void on_hw7slider_valueChanged(int value);

    void on_hw8slider_valueChanged(int value);

    double calculateBestPercent(std::string type);

    void on_midterm1slider_valueChanged(int value);

    void on_midterm2slider_valueChanged(int value);

    void on_finalexamslider_valueChanged(int value);

    void on_schemeAradioButton_clicked();

    void on_schemeBradioButton_clicked();

    void on_hw1slider_sliderMoved();

    void on_hw2slider_sliderMoved();

    void on_hw3slider_sliderMoved();

    void on_hw4slider_sliderMoved();

    void on_hw5slider_sliderMoved();

    void on_hw6slider_sliderMoved();

    void on_hw7slider_sliderMoved();

    void on_hw8slider_sliderMoved();

    void on_midterm1slider_sliderMoved();

    void on_midterm2slider_sliderMoved();

    void on_finalexamslider_sliderMoved();

    void on_hw1spinbox_valueChanged();

    void on_hw2spinbox_valueChanged();

    void on_hw3spinbox_valueChanged();

    void on_hw4spinbox_valueChanged();

    void on_hw5spinbox_valueChanged();

    void on_hw6spinbox_valueChanged();

    void on_hw7spinbox_valueChanged();

    void on_hw8spinbox_valueChanged();

    void on_midterm1spinbox_valueChanged();

    void on_midterm2spinbox_valueChanged();

    void on_finalexamspinbox_valueChanged();

    void on_PIC10B_button_clicked();

    void on_PIC10C_button_clicked();

private:
    Ui::MainWindow *ui;

    int hw1, hw2, hw3, hw4, hw5, hw6, hw7, hw8;

    int midterm_sum;

    int midterm1;

    int midterm2;

    int final;

    double final_project;

    bool clickedA, clickedB;

    bool pic10B_clicked, pic10C_clicked;
};

#endif // MAINWINDOW_H
